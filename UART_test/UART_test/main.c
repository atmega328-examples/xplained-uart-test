/*
 * UART_test.c
 *
 * Created: 17-02-2020 15:03:27
 * Author : npes
 */ 

#define F_CPU 8000000UL //use 8MHz clock
#define BAUD 9600 //BAUD Rate for UART
#include <stdio.h> // Needed for printf + scanf
#include <avr/io.h> // avr macros
#include "Library/STDIO_UART.h" // Needed for stdio to UART redirection
#include <util/delay.h> // needed for blocking delay

int main(void)
{
	ioinit(); //Setup UART and STDIO
	int counter = 0;
	
    /* Replace with your application code */
    while (1) 
    {
		printf("From Xplained %d \n", counter);
		counter++;
		_delay_ms(1000);		
    }
}

